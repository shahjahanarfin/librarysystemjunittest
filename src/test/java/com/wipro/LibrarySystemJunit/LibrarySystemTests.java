package com.wipro.LibrarySystemJunit;

import java.util.List;
import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.wipro.LibrarySystemJunit.entity.LibraryBook;
import com.wipro.LibrarySystemJunit.repository.LibrarySystemRepo;

@DataJpaTest
public class LibrarySystemTests {
	@Autowired
	private LibrarySystemRepo librepo;
	
	@Test
	public void saveBookTest() {
		LibraryBook libbook = LibraryBook.builder()
				.bookName("Gulliver\'s Travel")
				.authorName("Anonymous")
				.cost(2000)
				.build();
		librepo.save(libbook);
		Assertions.assertThat(libbook.getId()).isGreaterThan(0);
	}
	
	@Test
	public void getBookTest(){
		LibraryBook libbook = librepo.findById(1L).get();
        Assertions.assertThat(libbook.getId()).isEqualTo(0);
    }
	
	@Test
	public void getListOfBookTest(){
		List<LibraryBook> libbook = librepo.findAll();
        Assertions.assertThat(libbook.size()).isGreaterThan(0);
    }
	
	@Test
	public void updateLibraryBookTest(){
        LibraryBook libbook = librepo.findById(1L).get();
        libbook.setAuthorName("Alber Gosh");
        LibraryBook libbookupdated =  librepo.save(libbook);
        Assertions.assertThat(libbookupdated.getAuthorName()).isEqualTo("Alber Gosh");
    }
	
	@Test
	public void deleteEmployeeTest(){
        LibraryBook libbook = librepo.findById(1L).get();
        librepo.delete(libbook);        LibraryBook libbook1 = null;
        Optional<LibraryBook> optionalBook = librepo.findById(1L);
        if(optionalBook.isPresent()){
            libbook1 = optionalBook.get();
        }
        Assertions.assertThat(libbook1).isNull();
    }
}
